# login-valorant
AutoHotKey script that allow to login to a valorant account. I personnally use it with my smurfes to quick launch the account I want to play on at a specific moment form my desktop:

![windows context menu valorant login](https://gitlab.com/letchepare/login-valorant/-/raw/master/valorant_autologin_submenu.png)

# How do I use this script ?
Run the script in cmd by calling `autohotkey.exe "<Path to the valorant autologin.ahk file>" <login> <pass>`